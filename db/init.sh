#!/bin/bash
set -e

# DEFAULT_OWNER=$(psql -v ON_ERROR_STOP=1 --username="postgres" --dbname=template1 --tuples-only --no-align --command="SELECT schema_owner FROM information_schema.schemata WHERE schema_name = 'public'")

psql -v ON_ERROR_STOP=1 --username="postgres" --dbname=template1 <<-EOSQL
-- Configure Pharos database
CREATE USER ${PHAROS_DB_USER} CREATEDB;
ALTER USER ${PHAROS_DB_USER} WITH PASSWORD '${PHAROS_DB_PASS}';
-- ALTER SCHEMA public OWNER to ${PHAROS_DB_USER};
CREATE DATABASE ${PHAROS_DB_NAME} OWNER ${PHAROS_DB_USER};

-- ALTER SCHEMA public OWNER to ${DEFAULT_OWNER};
EOSQL

