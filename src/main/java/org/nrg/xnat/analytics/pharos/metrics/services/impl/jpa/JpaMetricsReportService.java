package org.nrg.xnat.analytics.pharos.metrics.services.impl.jpa;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.repositories.MetricsReportRepository;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;

import static lombok.AccessLevel.PRIVATE;

@Service
@Transactional
@Getter(PRIVATE)
@Setter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class JpaMetricsReportService implements MetricsReportService {
    private final MetricsReportRepository _reportRepository;
    private final ObjectMapper            _mapper;

    /**
     * Initializes the metrics report service.
     *
     * @param reportRepository The JPA repository for managing {@link MetricsReport metrics reports}
     * @param objectMapper     The application object mapper instance.
     */
    @Autowired
    public JpaMetricsReportService(final MetricsReportRepository reportRepository, final ObjectMapper objectMapper) {
        _reportRepository = reportRepository;
        _mapper           = objectMapper;
        log.info("Created Metrics report service");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetricsReport create(final MetricsReport report) {
        final MetricsReport persisted = getReportRepository().saveAndFlush(report);
        log.debug("Saved report with ID {}", persisted.getId());
        return persisted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetricsReport get(final long id) throws NoResultException {
        return getReportRepository().readById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MetricsReport> getAll() {
        return getAll(Pageable.unpaged()).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<MetricsReport> getAll(final Pageable page) {
        return getReportRepository().findAll(page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MetricsReport> findByUuid(final String uuid) {
        return findByUuid(uuid, Pageable.unpaged()).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<MetricsReport> findByUuid(final String guid, final Pageable page) {
        return getReportRepository().findAllByUuid(UUID.fromString(guid), page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MetricsReport> findByUuid(final UUID guid) {
        return findByUuid(guid, Pageable.unpaged()).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<MetricsReport> findByUuid(final UUID guid, final Pageable page) {
        return getReportRepository().findAllByUuid(guid, page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MetricsReport> findByIpAddress(final String ipAddress) {
        return findByIpAddress(ipAddress, Pageable.unpaged()).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<MetricsReport> findByIpAddress(final String ipAddress, final Pageable page) {
        return getReportRepository().findAllByIpAddress(ipAddress, page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MetricsReport> findByXnatVersion(final String xnatVersion) {
        return findByXnatVersion(xnatVersion, Pageable.unpaged()).getContent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<MetricsReport> findByXnatVersion(final String xnatVersion, final Pageable page) {
        return getReportRepository().findAllByXnatVersion(xnatVersion, page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(final long id) {
        getReportRepository().deleteById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(final MetricsReport report) {
        getReportRepository().delete(report);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long deleteAll() {
        final long number = getReportRepository().count();
        getReportRepository().deleteAll();
        return number;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String serialize(final MetricsReport report) {
        try {
            return _mapper.writeValueAsString(report);
        } catch (JsonProcessingException e) {
            log.warn("Error encountered trying to serialize a metrics report", e);
            return "";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<MetricsReport> deserialize(final String report) {
        try {
            return Optional.of(_mapper.readValue(report, MetricsReport.class));
        } catch (JsonProcessingException e) {
            log.warn("Error encountered trying to deserialize a metrics report from value: {}", report, e);
            return Optional.empty();
        }
    }
}
