package org.nrg.xnat.analytics.pharos.metrics.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.analytics.pharos.core.NotFoundException;
import org.nrg.xnat.analytics.pharos.core.controllers.AbstractPharosApi;
import org.nrg.xnat.analytics.pharos.metrics.annotations.MetricsReportParam;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Metrics Collector", description = "Handles submissions of metrics data from clients")
@RestController
@RequestMapping("/metrics")
@Getter(PROTECTED)
@Setter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class MetricsCollectorApi extends AbstractPharosApi {
    private final MetricsReportService _service;

    @Autowired
    public MetricsCollectorApi(final MetricsReportService service) {
        _service = service;
        log.info("Created Metrics Collectors API");
    }

    @Operation(summary = "Gets all available metrics reports",
               parameters = {@Parameter(name = "page", in = ParameterIn.QUERY, description = "Indicates the page of data to be retrieved"),
                             @Parameter(name = "size", in = ParameterIn.QUERY, description = "Indicates the number of items in a page of data")})
    @ApiResponses({@ApiResponse(responseCode = "200", description = "The list of metrics reports was successfully returned"),
                   @ApiResponse(responseCode = "403", description = "The user doesn't have sufficient permissions to view the list of reports")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_SUBMITTER')")
    @PostFilter("hasAnyRole('ROLE_ADMIN', 'ROLE_USER') || filterObject.uuid == authentication.name")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<MetricsReport> getMetricsReports(final @RequestParam(required = false, defaultValue = "0") int page,
                                                 final @RequestParam(required = false, defaultValue = "0") int size) {
        // FIXME: 2/15/22 This returns a list rather than page because of the @PostFilter. It should use the Spring Data integration
        //  as described here: https://docs.spring.io/spring-security/reference/features/integrations/data.html
        return new ArrayList<>(getService().getAll(page(page, size)).getContent());
    }

    @Operation(summary = "Deletes all metrics reports")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "All metrics reports were successfully deleted"),
                   @ApiResponse(responseCode = "403", description = "The user doesn't have sufficient permissions to delete all reports")})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping
    public long deleteAllMetricsReports() {
        return getService().deleteAll();
    }

    @ApiResponse(responseCode = "200", description = "Successful operation",
                 content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = MetricsReport.class)))
    @ApiResponse(responseCode = "405", description = "Invalid input")
    @PreAuthorize("isFullyAuthenticated() && hasRole('ROLE_SUBMITTER')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public MetricsReport handleMetricsReport(final @MetricsReportParam MetricsReport report) {
        // TODO: I tried to deal with this in the @PreAuthorize above with this: " && #report.uuid == authentication.name". However, that causes an error
        //  because the package for this class is not exported to unnamed module.
        final Optional<UserDetails> user = getUser();
        if (user.isEmpty()) {
            // TODO: This shouldn't happen because it requires being fully authenticated
            throw new InsufficientAuthenticationException("Must authenticate with UUID and access token to submit a metrics report");
        }
        if (!StringUtils.equals(user.get().getUsername(), report.getUuid().toString())) {
            throw new InsufficientAuthenticationException("Report UUID must match submitter's UUID");
        }
        return getService().create(report);
    }

    @ApiResponse(responseCode = "200", description = "Successful operation",
                 content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = MetricsReport.class)))
    @ApiResponse(responseCode = "405", description = "Invalid input")
    @PostAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER') || returnObject.uuid == authentication.name")
    @GetMapping(path = "id/{id}", produces = APPLICATION_JSON_VALUE)
    public MetricsReport getMetricsReport(final @PathVariable long id) {
        return Optional.of(getService().get(id)).orElseThrow(() -> new NotFoundException(MetricsReport.class, id));
    }

    @ApiResponse(responseCode = "200", description = "Successful operation",
                 content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = MetricsReport.class)))
    @ApiResponse(responseCode = "405", description = "Invalid input")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER') || #uuid == authentication.name")
    @GetMapping(path = "uuid/{uuid}", produces = APPLICATION_JSON_VALUE)
    public Page<MetricsReport> getMetricsReportsByGuid(final @PathVariable String uuid,
                                                       final @RequestParam(required = false, defaultValue = "0") int page,
                                                       final @RequestParam(required = false, defaultValue = "0") int size) {
        return getService().findByUuid(uuid, page(page, size));
    }

    @ApiResponse(responseCode = "200", description = "Successful operation",
                 content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = MetricsReport.class)))
    @ApiResponse(responseCode = "405", description = "Invalid input")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_SUBMITTER')")
    @PostFilter("hasAnyRole('ROLE_ADMIN', 'ROLE_USER') || filterObject.uuid == authentication.name")
    @GetMapping(path = "ip/{ipAddress}", produces = APPLICATION_JSON_VALUE)
    public List<MetricsReport> getMetricsReportsByIpAddress(final @PathVariable String ipAddress,
                                                            final @RequestParam(required = false, defaultValue = "0") int page,
                                                            final @RequestParam(required = false, defaultValue = "0") int size) {
        return new ArrayList<>(getService().findByIpAddress(ipAddress, page(page, size)).getContent());
    }

    @ApiResponse(responseCode = "200", description = "Successful operation",
                 content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = MetricsReport.class)))
    @ApiResponse(responseCode = "405", description = "Invalid input")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_SUBMITTER')")
    @PostFilter("hasAnyRole('ROLE_ADMIN', 'ROLE_USER') || filterObject.uuid == authentication.name")
    @GetMapping(path = "version/{xnatVersion}", produces = APPLICATION_JSON_VALUE)
    public List<MetricsReport> getMetricsReportsByXnatVersion(final @PathVariable String xnatVersion,
                                                              final @RequestParam(required = false, defaultValue = "0") int page,
                                                              final @RequestParam(required = false, defaultValue = "0") int size) {
        return new ArrayList<>(getService().findByXnatVersion(xnatVersion, page(page, size)).getContent());
    }
}
