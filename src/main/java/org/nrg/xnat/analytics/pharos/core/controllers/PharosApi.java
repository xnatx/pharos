package org.nrg.xnat.analytics.pharos.core.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.nrg.xnat.analytics.pharos.core.domain.User;
import org.nrg.xnat.analytics.pharos.core.services.PharosUserDetailsManager;
import org.nrg.xnat.analytics.pharos.core.utilities.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;
import static org.nrg.xnat.analytics.pharos.core.services.impl.jpa.JpaPharosUserDetailsManager.ATTRIBUTE_IP_ADDRESS;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Pharos", description = "This is the primary Pharos API")
@RestController
@RequestMapping("/app")
@Getter(PROTECTED)
@Setter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class PharosApi extends AbstractPharosApi {
    private static final String       GREETING          = "Hello, I'm Pharos %s running in mode(s) \"%s\"\n"
                                                          + " * OS:   %s\n"
                                                          + " * Java: %s\n";
    private static final String       ANONYMOUS         = "%s\nYou are anonymous.\n";
    private static final String       AUTHENTICATED     = "%s\nYou are:\n"
                                                          + " * Username:    %s\n"
                                                          + " * Groups:      %s\n"
                                                          + " * Authorities: %s\n";
    private static final List<String> JAVA_INFO         = Arrays.asList("java.vm.name", "java.version", "java.vendor", "java.vendor.version");
    private static final List<String> OS_INFO           = Arrays.asList("os.name", "os.version", "os.arch");
    private static final String       NO_GROUPS_MESSAGE = "<none>";

    private final PharosUserDetailsManager   _userDetailsManager;
    private final NamedParameterJdbcTemplate _template;
    private final String                     _activeProfiles;
    private final String                     _osInfo;
    private final String                     _javaInfo;

    @Value("${info.app.version:unknown}")
    private String _pharosVersion;

    @Value("${server.tomcat.basedir:/tmp}")
    private String _tomcatBasedir;

    @Value("${server.tomcat.accesslog.directory:logs}")
    private String _accessLogDirectory;

    @Value("${server.tomcat.accesslog.enabled:false}")
    private boolean _accessLogEnabled;

    @Autowired
    public PharosApi(final PharosUserDetailsManager userDetailsManager, final Environment environment, final DataSource dataSource) {
        log.info("Created Pharos API");
        _userDetailsManager = userDetailsManager;
        _template           = new NamedParameterJdbcTemplate(dataSource);
        _activeProfiles     = String.join(", ", environment.getActiveProfiles());
        _osInfo             = format(OS_INFO);
        _javaInfo           = format(JAVA_INFO);
    }

    @Operation(summary = "Get the application info", description = "Returns information about the Pharos application", tags = {"metadata", "pharos"})
    @ApiResponses(@ApiResponse(responseCode = "200", description = "The application info was successfully returned"))
    @PreAuthorize("permitAll()")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public String getAppInfo() {
        return getUser().map(this::getUserMessage).orElseGet(this::getAnonymousUserMessage);
    }

    @Operation(summary = "Register a new XNAT system with Pharos", description = "Doesn't do much now", tags = {"metadata", "pharos", "register"})
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "The registration was completed and a new UUID was successfully returned"),
                           @ApiResponse(responseCode = "405", description = "Invalid input")})
    @PreAuthorize("isAnonymous()")
    @PostMapping(path = "register", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Pair<UUID, UUID> registerNewClient(final HttpServletRequest request, final @RequestBody Map<String, String> parameters) {
        final String ipAddress = RequestUtils.getIpAddressFromRequest(request);
        parameters.put(ATTRIBUTE_IP_ADDRESS, ipAddress);

        final Pair<UUID, UUID> uuidAndAccessToken = _userDetailsManager.createSubmitter(parameters);
        return ImmutablePair.of(uuidAndAccessToken.getKey(), uuidAndAccessToken.getValue());
    }

    @Operation(summary = "Get a list of active submitters and their IP addresses")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successfully retrieved all active submitters"),
                           @ApiResponse(responseCode = "405", description = "Invalid input")})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @GetMapping(path = "submitters", produces = APPLICATION_JSON_VALUE)
    public Map<String, Map<String, String>> getSubmitters() {
        return _userDetailsManager.getActiveSubmitters().stream().collect(Collectors.toMap(Submitter::getUuid, PharosApi::mapSubmitterProperties));
    }

    @Operation(summary = "Deletes all submitters")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successfully deleted all submitters"),
                           @ApiResponse(responseCode = "405", description = "Invalid input")})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "submitters", produces = APPLICATION_JSON_VALUE)
    public int deleteSubmitters() {
        return _userDetailsManager.deleteAllSubmitters();
    }

    @SuppressWarnings({"SqlNoDataSourceInspection", "SqlResolve", "SqlDialectInspection"})
    @Operation(summary = "Get a list of enabled users")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successfully retrieved all enabled users"),
                           @ApiResponse(responseCode = "405", description = "Invalid input")})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(path = "users", produces = APPLICATION_JSON_VALUE)
    public List<String> getUsers() {
        return _template.queryForList("SELECT username FROM users WHERE enabled = true", EmptySqlParameterSource.INSTANCE, String.class);
    }

    @Operation(summary = "Get info about Tomcat access logging")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successfully retrieved Tomcat access logging info"),
                           @ApiResponse(responseCode = "405", description = "Invalid input")})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(path = "access", produces = APPLICATION_JSON_VALUE)
    public String getAccessLogInfo() {
        final Path accessLogDirectory = Paths.get(_accessLogDirectory);
        return (_accessLogEnabled ? "" : "Accessing logging disabled\n")
               + "Access log folder: " +
               (accessLogDirectory.isAbsolute() ? accessLogDirectory : Paths.get(_tomcatBasedir, _accessLogDirectory));
    }

    private String getUserMessage(final UserDetails user) {
        final String groups;
        if (user instanceof User) {
            groups = ((User) user).getGroups().stream().map(Group::getGroupName).collect(Collectors.joining(", "));
        } else {
            groups = NO_GROUPS_MESSAGE;
        }
        final String authorities = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(", "));
        return String.format(AUTHENTICATED, getGreeting(), user.getUsername(), groups, authorities);
    }

    private String getAnonymousUserMessage() {
        return String.format(ANONYMOUS, getGreeting());
    }

    private String getGreeting() {
        return String.format(GREETING, _pharosVersion, _activeProfiles, _osInfo, _javaInfo);
    }

    private static Map<String, String> mapSubmitterProperties(final Submitter submitter) {
        final Map<String, String> properties = new HashMap<>();
        if (StringUtils.isNotBlank(submitter.getContactName())) {
            properties.put("name", submitter.getContactName());
        }
        if (StringUtils.isNotBlank(submitter.getContactEmail())) {
            properties.put("email", submitter.getContactEmail());
        }
        if (StringUtils.isNotBlank(submitter.getContactTelephone())) {
            properties.put("telephone", submitter.getContactTelephone());
        }
        if (StringUtils.isNotBlank(submitter.getContactUrl())) {
            properties.put("url", submitter.getContactUrl());
        }
        if (StringUtils.isNotBlank(submitter.getSiteUrl())) {
            properties.put("site", submitter.getSiteUrl());
        }
        return properties;
    }

    private static String format(final List<String> properties) {
        return properties.stream().map(System::getProperty).collect(Collectors.joining(" "));
    }
}
