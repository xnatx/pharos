package org.nrg.xnat.analytics.pharos.crashpad.resolvers;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.core.resolvers.AbstractHandlerMethodArgumentResolver;
import org.nrg.xnat.analytics.pharos.crashpad.annotations.CrashReportParam;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

@Component
@Slf4j
public class CrashReportRequestResolver extends AbstractHandlerMethodArgumentResolver<CrashReportParam, CrashReport> {
    private static final String KEY_GUID             = "guid";
    private static final String KEY_PRODUCT_NAME     = "_productName";
    private static final String KEY_PRODUCT_VERSION  = "_version";
    private static final String KEY_PLATFORM         = "platform";
    private static final String KEY_PROCESS_TYPE     = "process_type";
    private static final String KEY_ELECTRON_VERSION = "ver";
    private static final String KEY_MINIDUMP         = "upload_file_minidump";

    @Autowired
    public CrashReportRequestResolver() {
        super(CrashReportParam.class);
        log.info("Created Crashpad report request resolver");
    }

    @Override
    protected CrashReport handleArgument(final HttpServletRequest request,
                                         final Map<String, String[]> parameters,
                                         final Map<String, List<MultipartFile>> files) {
        return CrashReport.builder()
                          .guid(getJoinedParameters(parameters, KEY_GUID))
                          .productName(getJoinedParameters(parameters, KEY_PRODUCT_NAME))
                          .productVersion(getJoinedParameters(parameters, KEY_PRODUCT_VERSION))
                          .platform(getJoinedParameters(parameters, KEY_PLATFORM))
                          .processType(getJoinedParameters(parameters, KEY_PROCESS_TYPE))
                          .electronVersion(getJoinedParameters(parameters, KEY_ELECTRON_VERSION))
                          .dumpFile(convertMultipartFilesToArray(files.get(KEY_MINIDUMP)))
                          .build();
    }
}
