package org.nrg.xnat.analytics.pharos.core.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class PharosAccessDeniedHandler implements AccessDeniedHandler {
    private static final String UNKNOWN = "unknown or anonymous user";

    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response, final AccessDeniedException e) throws IOException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("Attempt by {} to access protected resource: {}", authentication != null ? authentication.getName() : UNKNOWN, request.getRequestURI());
        response.sendRedirect(request.getContextPath() + "/access-denied");
    }
}
