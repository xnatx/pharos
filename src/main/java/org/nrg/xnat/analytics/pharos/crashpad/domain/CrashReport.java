package org.nrg.xnat.analytics.pharos.crashpad.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Immutable;
import org.nrg.xnat.analytics.pharos.core.domain.ImmutableEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Map;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.Pattern;

import static lombok.AccessLevel.PROTECTED;
import static lombok.AccessLevel.PUBLIC;

/*
CrashReporter payload from https://electronjs.org/docs/api/crash-reporter

ver String - The version of Electron.
platform String - e.g. 'win32'.
process_type String - e.g. 'renderer'.
guid String - e.g. '5e1286fc-da97-479e-918b-6bfb0c3d1c72'.
_version String - The version in package.json.
_productName String - The product name in the crashReporter options object.
prod String - Name of the underlying product. In this case Electron.
_companyName String - The company name in the crashReporter options object.
upload_file_minidump File - The crash report in the format of minidump.
All first-level properties of the extra object in the crashReporter options object.
 */
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "crash_reports")
@Immutable
@Getter(PUBLIC)
@Setter(PROTECTED)
@SuperBuilder
@AllArgsConstructor
@RequiredArgsConstructor
@JsonPropertyOrder({"id", "productName", "productVersion", "platform", "processType", "electronVersion", "guid", "created", "createdBy", "extra", "dumpFile"})
public class CrashReport extends ImmutableEntity {
    @Pattern(regexp = "^(?i:[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?)$")
    private String              guid; // guid String - e.g. '5e1286fc-da97-479e-918b-6bfb0c3d1c72'.
    private String              productName; // _productName String - The product name in the crashReporter options object.
    private String              productVersion; // _version String - The version in package.json.
    private String              electronVersion; // ver - The version of Electron.
    private String              platform; // platform String - e.g. 'win32'.
    private String              processType; // process_type String - e.g. 'renderer'.
    @Lob
    private byte[]              dumpFile;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "crash_report_parameters", joinColumns = @JoinColumn(name = "id"))
    @MapKeyColumn(name = "parameterName")
    @Column(name = "parameterValue", columnDefinition = "TEXT")
    private Map<String, String> extra; // All first-level properties of the extra object in the crashReporter options object.

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CrashReport)) {
            return false;
        }
        if (!super.equals(object)) {
            return false;
        }
        final CrashReport that = (CrashReport) object;
        return getGuid().equals(that.getGuid())
               && getProductName().equals(that.getProductName())
               && getProductVersion().equals(that.getProductVersion())
               && getElectronVersion().equals(that.getElectronVersion())
               && getPlatform().equals(that.getPlatform())
               && getProcessType().equals(that.getProcessType())
               && getExtra().equals(that.getExtra());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), guid, productName, productVersion, electronVersion, platform, processType, extra);
    }
}
