package org.nrg.xnat.analytics.pharos.core.services;

import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.nrg.xnat.analytics.pharos.core.domain.User;

import java.util.List;
import java.util.Optional;

public interface PharosUserService {
    /**
     * Saves the submitted {@link User user} to the database.
     *
     * @param user The user to save
     *
     * @return The saved and refreshed user object.
     */
    User save(User user);

    /**
     * Saves the submitted {@link Group group} to the database.
     *
     * @param group The group to save
     *
     * @return The saved and refreshed group object.
     */
    Group save(Group group);

    /**
     * Saves the submitted {@link Submitter submitter} to the database.
     *
     * @param submitter The submitter to save
     *
     * @return The saved and refreshed submitter object.
     */
    Submitter save(Submitter submitter);

    /**
     * Retrieves all users from the database.
     *
     * @return A list of users in the database.
     */
    List<User> findAllUsers();

    /**
     * Retrieves all groups from the database.
     *
     * @return A list of groups in the database.
     */
    List<Group> findAllGroups();

    /**
     * Retrieves all submitters from the database.
     *
     * @return A list of submitters in the database.
     */
    List<Submitter> findAllSubmitters();

    /**
     * Retrieves all <i>active</i> submitters from the database.
     *
     * @return A list of active submitters in the database.
     */
    List<Submitter> findActiveSubmitters();

    /**
     * Retrieves the user with the specified name.
     *
     * @param username The username for the user to be retrieved.
     *
     * @return An optional with the user with the specified name if it exists.
     */
    Optional<User> findUserByName(String username);

    /**
     * Retrieves the group with the specified name.
     *
     * @param groupName The name for the group to be retrieved.
     *
     * @return An optional with the group with the specified name if it exists.
     */
    Optional<Group> findGroupByName(String groupName);

    /**
     * Retrieves the submitter with the specified UUID.
     *
     * @param uuid The UUID for the submitter to be retrieved.
     *
     * @return An optional with the submitter with the specified name if it exists.
     */
    Optional<Submitter> findSubmitterByUuid(String uuid);

    /**
     * Indicates whether the user with the specified name exists in the system.
     *
     * @param name The username for the user to check.
     *
     * @return Returns true if a user with the specified name exists in the system, false otherwise.
     */
    boolean existsUserByName(String name);

    /**
     * Indicates whether the group with the specified name exists in the system.
     *
     * @param name The name for the group to check.
     *
     * @return Returns true if a group with the specified name exists in the system, false otherwise.
     */
    boolean existsGroupByName(String name);

    /**
     * Indicates whether the submitter with the specified UUID exists in the system.
     *
     * @param uuid The UUID for the submitter to check.
     *
     * @return Returns true if a submitter with the specified UUID exists in the system, false otherwise.
     */
    boolean existsSubmitterByUuid(String uuid);

    /**
     * Deletes the user with the specified name.
     *
     * @param username The name of the user to be deleted.
     */
    void deleteUserByName(String username);

    /**
     * Deletes the group with the specified name.
     *
     * @param groupName The name of the group to be deleted.
     */
    void deleteGroupByName(String groupName);

    /**
     * Deletes the submitter with the specified UUID.
     *
     * @param uuid The UUID of the submitter to be deleted.
     */
    void deleteSubmitterByUuid(String uuid);

    /**
     * Adds the user to the group.
     *
     * @param username  The name of the user to add to the group.
     * @param groupName The name of the group to which the user should be added.
     */
    void addUserToGroup(final String username, final String groupName);

    /**
     * Removes the user from the group.
     *
     * @param username  The name of the user to be removed from the group.
     * @param groupName The name of the group from which the user should be removed.
     */
    void removeUserFromGroup(final String username, final String groupName);
}
