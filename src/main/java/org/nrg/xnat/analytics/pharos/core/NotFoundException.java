package org.nrg.xnat.analytics.pharos.core;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Represents an exception for an item that couldn't be located by type and ID.
 * This exception sets the response status to {@link HttpStatus#NOT_FOUND}.
 */
@Getter
@Accessors(prefix = "_")
public class NotFoundException extends ResponseStatusException {
    private final Class<?> _type;
    private final String   _id;

    /**
     * Creates a new exception for an item with the specified class and ID.
     *
     * @param type The class of the requested item
     * @param id   The ID of the requested item
     */
    public NotFoundException(final Class<?> type, final Number id) {
        this(type, id.toString());
    }

    /**
     * Creates a new exception for an item with the specified class and ID.
     *
     * @param type The class of the requested item
     * @param id   The ID of the requested item
     */
    public NotFoundException(final Class<?> type, final String id) {
        super(HttpStatus.NOT_FOUND, "Couldn't find " + type + " with ID " + id);
        _type = type;
        _id   = id;
    }
}
