package org.nrg.xnat.analytics.pharos.crashpad.services.impl.jpa;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.repositories.CrashReportRepository;
import org.nrg.xnat.analytics.pharos.crashpad.repositories.SimplifiedCrashReportRepository;
import org.nrg.xnat.analytics.pharos.crashpad.services.CrashReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

import static lombok.AccessLevel.PRIVATE;

@Service
@Transactional
@Getter(PRIVATE)
@Setter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class JpaCrashReportService implements CrashReportService {
    private final CrashReportRepository           _reportRepository;
    private final SimplifiedCrashReportRepository _simplifiedReportRepository;

    /**
     * Initializes the crash report service.
     *
     * @param reportRepository           The JPA repository for managing {@link CrashReport full crash reports}
     * @param simplifiedReportRepository The JPA repository for managing {@link SimplifiedCrashReport simplified crash reports}
     */
    @Autowired
    public JpaCrashReportService(final CrashReportRepository reportRepository, final SimplifiedCrashReportRepository simplifiedReportRepository) {
        log.info("Created Crashpad report service");
        _reportRepository           = reportRepository;
        _simplifiedReportRepository = simplifiedReportRepository;
    }

    @Override
    public CrashReport getFullCrashReport(final long id) {
        return getReportRepository().readById(id);
    }

    @Override
    public List<CrashReport> findReportsByGuid(final String guid) {
        return findReportsByGuid(guid, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<CrashReport> findReportsByGuid(final String guid, final Pageable page) {
        return getReportRepository().findAllByGuid(guid, page);
    }

    @Override
    public List<CrashReport> findReportsByProductName(final String productName) {
        return findReportsByProductName(productName, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<CrashReport> findReportsByProductName(final String productName, final Pageable page) {
        return getReportRepository().findAllByProductName(productName, page);
    }

    @Override
    public List<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion) {
        return findReportsByProductNameAndProductVersion(productName, productVersion, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion, final Pageable page) {
        return getReportRepository().findAllByProductNameAndProductVersion(productName, productVersion, page);
    }

    @Override
    public SimplifiedCrashReport get(final long id) {
        return getSimplifiedReportRepository().readById(id);
    }

    @Override
    public List<SimplifiedCrashReport> getAllSimplifiedReports() {
        return getAllSimplifiedReports(Pageable.unpaged()).getContent();
    }

    @Override
    public Page<SimplifiedCrashReport> getAllSimplifiedReports(final Pageable page) {
        return getSimplifiedReportRepository().findBy(page);
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid) {
        return findSimplifiedReportsByGuid(guid, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid, final Pageable page) {
        return getSimplifiedReportRepository().findAllByGuid(guid, page);
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName) {
        return findSimplifiedReportsByProductName(productName, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName, final Pageable page) {
        return getSimplifiedReportRepository().findAllByProductName(productName, page);
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion) {
        return findSimplifiedReportsByProductNameAndProductVersion(productName, productVersion, Pageable.unpaged()).getContent();
    }

    @Override
    public Page<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion,
                                                                                           final Pageable page) {
        return getSimplifiedReportRepository().findAllByProductNameAndProductVersion(productName, productVersion, page);
    }

    @Override
    public void create(final CrashReport report) {
        getReportRepository().save(report);
    }

    @Override
    public Optional<CrashReport> findOneByGuid(final String guid) {
        return getReportRepository().findOne(Example.of(CrashReport.builder().guid(guid).build()));
    }
}
