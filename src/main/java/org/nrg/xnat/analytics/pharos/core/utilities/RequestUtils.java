package org.nrg.xnat.analytics.pharos.core.utilities;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class RequestUtils {
    private RequestUtils() {
        // Prevent instantiation
    }

    public static String getIpAddressFromRequest(final HttpServletRequest request) {
        return StringUtils.substringBefore(StringUtils.getIfBlank(request.getHeader("X-Forwarded-For"), request::getRemoteAddr), ",");
    }
}
