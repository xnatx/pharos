package org.nrg.xnat.analytics.pharos.core.resolvers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.PharosApplication;
import org.nrg.xnat.analytics.pharos.core.domain.MutableEntity;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
public abstract class AbstractMutableEntityDeserializer<E extends MutableEntity> extends AbstractImmutableEntityDeserializer<E> {
    private static final List<String> ENTITY_FIELDS = Arrays.asList("lastModified", "lastModifiedBy");

    private final DateFormat _dateFormatter;

    protected AbstractMutableEntityDeserializer(final Class<E> entityClass) {
        super(entityClass);
        _dateFormatter = new SimpleDateFormat(PharosApplication.COMMON_DATE_FORMAT);
    }

    @Override
    protected boolean canHandleField(final String field) {
        return ENTITY_FIELDS.contains(field) || super.canHandleField(field);
    }

    protected boolean handle(final JsonParser parser, final DeserializationContext context, final E instance, final String field) throws IOException {
        if (super.canHandleField(field)) {
            return super.handle(parser, context, instance, field);
        }
        parser.nextToken();
        switch (field) {
            case "lastModified":
                final String lastModified = parser.getText();
                try {
                    instance.setLastModified(_dateFormatter.parse(lastModified));
                } catch (ParseException e) {
                    throw InvalidFormatException.from(parser, "Invalid date format", lastModified, Date.class);
                }
                break;
            case "lastModifiedBy":
                instance.setLastModifiedBy(parser.getText());
                break;
            default:
                log.warn("Unknown property for immutable entities: {}", field);
                return false;
        }
        return true;
    }
}
