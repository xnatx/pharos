package org.nrg.xnat.analytics.pharos.core.repositories;

import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "submitters", path = "submitters")
public interface SubmitterRepository extends JpaRepository<Submitter, Long> {
    Submitter findByUuid(final String uuid);

    boolean existsByUuid(final String uuid);

    void deleteByUuid(final String uuid);

    @Query("select s from Submitter s where s.active = true")
    List<Submitter> findActiveSubmitters();
}
