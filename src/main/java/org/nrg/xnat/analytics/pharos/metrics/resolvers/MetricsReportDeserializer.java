package org.nrg.xnat.analytics.pharos.metrics.resolvers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.core.resolvers.AbstractImmutableEntityDeserializer;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
public class MetricsReportDeserializer extends AbstractImmutableEntityDeserializer<MetricsReport> {
    public MetricsReportDeserializer() {
        super(MetricsReport.class);
    }

    @Override
    public MetricsReport deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw MismatchedInputException.from(parser, MetricsReport.class, "Invalid start marker");
        }
        final MetricsReport report = new MetricsReport();
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            final String field = parser.getCurrentName();
            if (canHandleField(field) && handle(parser, context, report, field)) {
                continue;
            }
            parser.nextToken();
            switch (field) {
                case "uuid":
                    report.setUuid(UUID.fromString(parser.getText()));
                    break;
                case "xnatVersion":
                    report.setXnatVersion(parser.getText());
                    break;
                case "ipAddress":
                    report.setIpAddress(parser.getText());
                    break;
                case "reports":
                case "providerReports":
                    if (parser.getCurrentToken() != JsonToken.START_ARRAY) {
                        throw MismatchedInputException.from(parser, MetricsReport.class,
                                                            "Invalid start marker for providerReports property: should be START_ARRAY");
                    }
                    final List<JsonNode> reports = new ArrayList<>();
                    while (parser.nextToken() != JsonToken.END_ARRAY) {
                        final JsonNode item = parser.readValueAsTree();
                        log.info("Got an object: {}", item);
                        reports.add(item);
                    }
                    report.setProviderReports(new ArrayNode(context.getNodeFactory(), reports));
                    break;
                default:
                    log.warn("Found an unknown property {} when trying to deserialize a MetricsReport", field);
            }
        }
        return report;
    }
}