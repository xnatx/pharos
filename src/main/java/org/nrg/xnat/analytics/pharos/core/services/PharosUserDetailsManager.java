package org.nrg.xnat.analytics.pharos.core.services;

import org.apache.commons.lang3.tuple.Pair;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.springframework.security.provisioning.GroupManager;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface PharosUserDetailsManager extends UserDetailsManager, GroupManager {
    /**
     * Creates a new {@link Submitter} object from the initialized parameter instance.
     *
     * @param submitter The submitter to be created.
     *
     * @return The saved and initialized submitter object.
     */
    Submitter createSubmitter(final Submitter submitter);

    /**
     * Creates a new {@link Submitter} object from the attributes in the submitted map. Recognized attribute
     * names include:
     *
     * <ul>
     *     <li>ipAddress</li>
     *     <li>siteUrl</li>
     *     <li>name or contactName</li>
     *     <li>email or contactEmail</li>
     *     <li>telephone or contactTelephone</li>
     *     <li>url or contactUrl</li>
     * </ul>
     *
     * @param attributes The attributes to set for the new submitter to be created.
     *
     * @return The UUID and access token for the newly created submitter.
     */
    Pair<UUID, UUID> createSubmitter(final Map<String, String> attributes);

    /**
     * Gets a list of all {@link Submitter} active submitters.
     *
     * @return A list of all {@link Submitter} active submitters.
     */
    List<Submitter> getActiveSubmitters();

    /**
     * Deletes the specified submitter. This also deletes the corresponding user entry for the submitter.
     *
     * @param submitter The submitter to delete.
     */
    // FIXME: 3/8/22 These delete methods are only for development purposes. Remove them before release.
    void deleteSubmitter(final Submitter submitter);

    /**
     * Deletes all submitters and their corresponding user entries.
     *
     * @return Returns the number of submitters deleted from the system.
     */
    // FIXME: 3/8/22 These delete methods are only for development purposes. Remove them before release.
    int deleteAllSubmitters();
}
