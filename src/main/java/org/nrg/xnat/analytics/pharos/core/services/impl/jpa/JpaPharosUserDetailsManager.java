package org.nrg.xnat.analytics.pharos.core.services.impl.jpa;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nrg.xnat.analytics.pharos.core.configuration.SecurityConfig;
import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.nrg.xnat.analytics.pharos.core.domain.User;
import org.nrg.xnat.analytics.pharos.core.services.PharosUserDetailsManager;
import org.nrg.xnat.analytics.pharos.core.services.PharosUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import static org.nrg.xnat.analytics.pharos.core.configuration.SecurityConfig.SUBMITTER_GROUP;

@Service("userDetailsManager")
@Transactional
@Slf4j
public class JpaPharosUserDetailsManager implements PharosUserDetailsManager {
    public static final String ATTRIBUTE_IP_ADDRESS        = "ipAddress";
    public static final String ATTRIBUTE_EMAIL             = "email";
    public static final String ATTRIBUTE_NAME              = "name";
    public static final String ATTRIBUTE_TELEPHONE         = "telephone";
    public static final String ATTRIBUTE_URL               = "url";
    public static final String ATTRIBUTE_CONTACT_EMAIL     = "contactEmail";
    public static final String ATTRIBUTE_CONTACT_NAME      = "contactName";
    public static final String ATTRIBUTE_CONTACT_TELEPHONE = "contactTelephone";
    public static final String ATTRIBUTE_CONTACT_URL       = "contactUrl";
    public static final String ATTRIBUTE_SITE_URL          = "siteUrl";

    private final PharosUserService _userService;
    private final PasswordEncoder   _encoder;

    @Autowired
    public JpaPharosUserDetailsManager(final PharosUserService userService, final PasswordEncoder encoder) {
        _userService = userService;
        _encoder     = encoder;
    }

    /**
     * Initializes user entries for any entry in the submitters table that aren't already in the user table.
     */
    @PostConstruct
    public void init() {
        final Group submitters = submitters();
        _userService.findAllSubmitters()
                    .stream()
                    .filter(submitter -> !userExists(submitter.getUuid()))
                    .forEach(submitter -> _userService.save(User.builder()
                                                                .username(submitter.getUuid())
                                                                .password(_encoder.encode(submitter.getAccessToken()))
                                                                .group(submitters)
                                                                .build()));
        log.info("Setting user provisional passwords");
        SecurityConfig.DEFAULT_USERS.keySet().stream().sorted().map(_userService::findUserByName).filter(Optional::isPresent).map(Optional::get).forEach(user -> {
            final String password = RandomStringUtils.randomAlphanumeric(16);
            user.setPassword(_encoder.encode(password));
            _userService.save(user);
            log.info(" * {}: {}", user.getUsername(), password);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Submitter createSubmitter(final Submitter submitter) {
        final String username = submitter.getUuid();
        log.info("Handling new submitter: {}", username);
        createUser(User.builder().username(username).password(submitter.getAccessToken()).group(submitters()).build());
        return _userService.save(submitter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<UUID, UUID> createSubmitter(final Map<String, String> attributes) {
        final UUID   uuid     = UUID.randomUUID();
        final String username = uuid.toString();
        final UUID   token    = UUID.randomUUID();

        final Submitter.SubmitterBuilder<?, ?> builder = Submitter.builder().uuid(username).accessToken(_encoder.encode(token.toString()));
        getParameter(attributes, ATTRIBUTE_IP_ADDRESS).ifPresent(builder::ipAddress);
        getParameter(attributes, ATTRIBUTE_EMAIL, ATTRIBUTE_CONTACT_EMAIL).ifPresent(builder::contactEmail);
        getParameter(attributes, ATTRIBUTE_NAME, ATTRIBUTE_CONTACT_NAME).ifPresent(builder::contactName);
        getParameter(attributes, ATTRIBUTE_TELEPHONE, ATTRIBUTE_CONTACT_TELEPHONE).ifPresent(builder::contactTelephone);
        getParameter(attributes, ATTRIBUTE_URL, ATTRIBUTE_CONTACT_URL).ifPresent(builder::contactUrl);
        getParameter(attributes, ATTRIBUTE_SITE_URL).ifPresent(builder::siteUrl);

        final Submitter submitter = createSubmitter(builder.build());
        log.info("Created new submitter with record ID {} for UUID {} at IP {}", submitter.getId(), submitter.getUuid(), submitter.getIpAddress());

        return Pair.of(uuid, token);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Submitter> getActiveSubmitters() {
        return _userService.findActiveSubmitters();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSubmitter(final Submitter submitter) {
        _userService.deleteSubmitterByUuid(submitter.getUuid());
        _userService.deleteUserByName(submitter.getUuid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAllSubmitters() {
        final List<Submitter> submitters = _userService.findAllSubmitters();
        submitters.forEach(this::deleteSubmitter);
        return submitters.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> findAllGroups() {
        return _userService.findAllGroups().stream().map(Group::getGroupName).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> findUsersInGroup(final String groupName) {
        return _userService.findGroupByName(groupName)
                           .map(group -> group.getUsers().stream().map(User::getUsername).collect(Collectors.toList()))
                           .orElse(Collections.emptyList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createGroup(final String groupName, final List<GrantedAuthority> authorities) {
        _userService.save(Group.builder()
                               .groupName(groupName)
                               .authorities(authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                               .build());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteGroup(final String groupName) {
        _userService.deleteGroupByName(groupName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void renameGroup(final String oldName, final String newName) {
        _userService.findGroupByName(oldName).ifPresent(group -> {
            group.setGroupName(newName);
            _userService.save(group);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addUserToGroup(final String username, final String groupName) {
        _userService.addUserToGroup(username, groupName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeUserFromGroup(final String username, final String groupName) {
        _userService.removeUserFromGroup(username, groupName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GrantedAuthority> findGroupAuthorities(final String groupName) {
        final Optional<List<GrantedAuthority>> auths = _userService.findGroupByName(groupName)
                                                                   .map(group -> group.getAuthorities().stream()
                                                                                      .map(SimpleGrantedAuthority::new)
                                                                                      .collect(Collectors.toList()));
        return auths.orElse(Collections.emptyList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addGroupAuthority(final String groupName, final GrantedAuthority authority) {
        _userService.findGroupByName(groupName).ifPresent(group -> {
            if (group.getAuthorities().stream().noneMatch(auth -> authority.getAuthority().equals(auth))) {
                group.getAuthorities().add(authority.getAuthority());
                _userService.save(group);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeGroupAuthority(final String groupName, final GrantedAuthority authority) {
        _userService.findGroupByName(groupName)
                    .ifPresent(group -> group.getAuthorities().stream()
                                             .filter(auth -> authority.getAuthority().equals(auth))
                                             .findFirst()
                                             .ifPresent(auth -> {
                                                 group.getAuthorities().remove(auth);
                                                 _userService.save(group);
                                             }));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createUser(final UserDetails user) {
        _userService.save((User) user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUser(final UserDetails user) {
        _userService.save((User) user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUser(final String username) {
        _userService.deleteUserByName(username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changePassword(final String oldPassword, final String newPassword) {
        final Authentication user = SecurityContextHolder.getContext().getAuthentication();
        if (user == null) {
            throw new AccessDeniedException("Can't change password as no Authentication object found in context for current user.");
        }
        final String username = user.getName();
        log.debug("Changing password for user '{}'", username);
        _userService.findUserByName(username).ifPresent(found -> {
            found.setPassword(newPassword);
            _userService.save(found);
            final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(found, null, found.getAuthorities());
            newAuthentication.setDetails(user.getDetails());

            final SecurityContext context = SecurityContextHolder.createEmptyContext();
            context.setAuthentication(newAuthentication);
            SecurityContextHolder.setContext(context);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean userExists(final String username) {
        return _userService.existsUserByName(username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final User user = _userService.findUserByName(username).orElseThrow(() -> new UsernameNotFoundException(username));
        log.debug("Found user {} with {} groups and {} authorities", username, user.getGroups().size(), user.getGroups().size());
        return user;
    }

    private Group submitters() {
        return _userService.findGroupByName(SUBMITTER_GROUP).orElseThrow(() -> new RuntimeException("Couldn't find the submitters group"));
    }

    private static Optional<String> getParameter(final Map<String, String> parameters, final String... aliases) {
        for (final String alias : aliases) {
            if (parameters.containsKey(alias)) {
                return Optional.of(parameters.get(alias));
            }
        }
        return Optional.empty();
    }
}
