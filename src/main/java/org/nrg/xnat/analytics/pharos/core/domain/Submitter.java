package org.nrg.xnat.analytics.pharos.core.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Immutable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "submitters")
@Immutable
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"uuid", "ipAddress", "contactEmail", "contactName", "contactTelephone", "contactUrl", "siteUrl", "active"})
@Slf4j
public class Submitter extends MutableEntity {
    @NonNull
    private String uuid;

    @JsonIgnore
    @NonNull
    private String accessToken;

    @NonNull
    private String ipAddress;

    @JsonAlias("email")
    private String contactEmail;

    @JsonAlias("name")
    private String contactName;

    @JsonAlias("telephone")
    private String contactTelephone;

    @JsonAlias("url")
    private String contactUrl;

    private String siteUrl;

    @Builder.Default
    private boolean active = true;
}
