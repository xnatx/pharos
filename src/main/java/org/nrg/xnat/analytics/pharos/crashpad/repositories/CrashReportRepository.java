package org.nrg.xnat.analytics.pharos.crashpad.repositories;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "crashreports", collectionResourceRel = "crashreports")
public interface CrashReportRepository extends JpaRepository<CrashReport, Long> {
    CrashReport readById(final long id);

    List<CrashReport> findAllByGuid(final String guid);

    Page<CrashReport> findAllByGuid(final String guid, final Pageable page);

    List<CrashReport> findAllByProductName(final String productName);

    Page<CrashReport> findAllByProductName(final String productName, final Pageable page);

    List<CrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion);

    Page<CrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion, final Pageable page);
}
