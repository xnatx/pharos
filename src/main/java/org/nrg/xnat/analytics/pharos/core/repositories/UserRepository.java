package org.nrg.xnat.analytics.pharos.core.repositories;

import org.nrg.xnat.analytics.pharos.core.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByUsername(final String username);

    User findByUsername(final String username);

    void deleteByUsername(final String username);
}
