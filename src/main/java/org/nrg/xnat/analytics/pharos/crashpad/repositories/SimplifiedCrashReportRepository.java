package org.nrg.xnat.analytics.pharos.crashpad.repositories;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "simplifiedcrashreports", path = "simplifiedcrashreports")
public interface SimplifiedCrashReportRepository extends JpaRepository<CrashReport, Long> {
    SimplifiedCrashReport readById(final long id);

    List<SimplifiedCrashReport> findBy();

    Page<SimplifiedCrashReport> findBy(final Pageable page);

    List<SimplifiedCrashReport> findAllByGuid(final String guid);

    Page<SimplifiedCrashReport> findAllByGuid(final String guid, final Pageable page);

    List<SimplifiedCrashReport> findAllByProductName(final String productName);

    Page<SimplifiedCrashReport> findAllByProductName(final String productName, final Pageable page);

    List<SimplifiedCrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion);

    Page<SimplifiedCrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion, final Pageable page);
}
