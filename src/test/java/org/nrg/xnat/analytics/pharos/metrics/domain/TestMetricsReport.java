package org.nrg.xnat.analytics.pharos.metrics.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.nrg.xnat.analytics.pharos.metrics.repositories.MetricsReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TestMetricsReport {
    private static final Pattern UUID_PATTERN       = Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$");
    private static final Pattern IP_ADDRESS_PATTERN = Pattern.compile("^([0-9]{1,3}\\.){3}[0-9]{1,3}$");
    private static final Pattern VERSION_PATTERN    = Pattern.compile("^1\\.[78]\\.[0-5]$");

    private final Random                  _random = new Random();
    private final MetricsReportRepository _reportRepository;
    private final ObjectMapper            _mapper;

    @Autowired
    public TestMetricsReport(final MetricsReportRepository reportRepository, final ObjectMapper objectMapper) {
        _reportRepository = reportRepository;
        _mapper           = objectMapper;
    }

    @Test
    public void testMetricsReportSerialization() throws JsonProcessingException {
        final String json = _mapper.writeValueAsString(generateMetricsReport());
        assertThat(json).isNotNull().isNotBlank();

        final JsonNode node = _mapper.readTree(json);
        assertThat(node.get("uuid").textValue()).isNotBlank().matches(UUID_PATTERN);
        assertThat(node.get("ipAddress").textValue()).isNotBlank().matches(IP_ADDRESS_PATTERN);
        assertThat(node.get("xnatVersion").textValue()).isNotBlank().matches(VERSION_PATTERN);

        final JsonNode reports = node.get("providerReports");
        assertThat(reports).isNotNull().hasSize(2);

        final MetricsReport report = _mapper.readValue(json, MetricsReport.class);
        assertThat(report).isNotNull().hasFieldOrProperty("uuid").hasFieldOrProperty("ipAddress").hasFieldOrProperty("xnatVersion");
        assertThat(report.getUuid()).isNotNull().asString().matches(UUID_PATTERN);
        assertThat(report.getIpAddress()).isNotBlank().matches(IP_ADDRESS_PATTERN);
        assertThat(report.getXnatVersion()).isNotBlank().matches(VERSION_PATTERN);
        assertThat(report.getProviderReports()).isNotNull().isNotEmpty().hasSize(2);
    }

    @Test
    public void testMetricsQueries() throws InterruptedException {
        final Map<UUID, MetricsReport> reports =
            IntStream.range(0, 6).mapToObj(index -> generateMetricsReport()).collect(Collectors.toMap(MetricsReport::getUuid, Function.identity()));
        for (final MetricsReport report : reports.values()) {
            _reportRepository.saveAndFlush(report);
            // Sleep just a little so there's time between each report.
            Thread.sleep(100);
        }
        for (final UUID uuid : reports.keySet()) {
            // final MetricsReport       reference     = reports.get(uuid);
            final List<MetricsReport> reportsByUuid = _reportRepository.findAllByUuid(uuid);
            assertThat(reportsByUuid).isNotNull().isNotEmpty().hasSize(1);
            final MetricsReport report = reportsByUuid.get(0);
            assertThat(report).isNotNull().hasFieldOrPropertyWithValue("uuid", uuid);
        }

    }

    private MetricsReport generateMetricsReport() throws RuntimeException {
        final Map<String, Object> metricsReport =
            ImmutableMap.<String, Object>builder().put("providerId", "xnatx-metrics").put("providerVersion", "1.8.4").put("projects", 32).put("subjects", 1832)
                        .put("experiments", 18327).put("dataTypes", 32).put("users", 125)
                        .put("platformInfo", "CentOS 7.6, Tomcat 7.0.96, OpenJDK 64-Bit Server VM, PostgreSQL 10.11")
                        .put("plugins", ImmutableMap.of("xnat:dqr", "1.0"))
                        .put("experimentsByDataType", ImmutableMap.builder().put("xnat:mrSessionData", 4213)
                                                                  .put("xnat:petSessionData", 3163)
                                                                  .put("xnat:ctSessionData", 1712)
                                                                  .put("rad:radReadData", 7321)
                                                                  .put("xnatx:nihss", 1918).build()).build();
        final Map<String, Object> dqrReport =
            ImmutableMap.<String, Object>builder().put("providerId", "xnatx-dqr").put("providerVersion", "1.8.4").put("pacsCount", 6)
                        .put("totalStudiesSent", 3864).put("totalStudiesReceived", 1853).put("totalDataSent", "772.8 GB").put("totalDataReceived", "370.6 GB")
                        .put("totalQueries", 10258).put("totalFailedQueries", 128).put("averageResponseTimeInMs", 138).build();
        return MetricsReport.builder().uuid(UUID.randomUUID()).xnatVersion(generateVersion()).ipAddress(generateIpAddress())
                            .providerReports(_mapper.valueToTree(Arrays.asList(metricsReport, dqrReport))).build();
    }

    private String generateVersion() {
        return "1." + (_random.nextInt(2) + 7) + "." + _random.nextInt(6);
    }

    private String generateIpAddress() {
        return IntStream.range(0, 4).mapToObj(index -> _random.nextInt(256)).map(Objects::toString).collect(Collectors.joining("."));
    }
}
