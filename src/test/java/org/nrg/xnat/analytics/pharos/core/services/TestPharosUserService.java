package org.nrg.xnat.analytics.pharos.core.services;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.nrg.xnat.analytics.pharos.core.domain.Group;
import org.nrg.xnat.analytics.pharos.core.domain.Submitter;
import org.nrg.xnat.analytics.pharos.core.domain.User;
import org.nrg.xnat.analytics.pharos.core.services.impl.jpa.JpaPharosUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Import(JpaPharosUserService.class)
@Slf4j
public class TestPharosUserService {
    private final PharosUserService _userService;

    @Autowired
    public TestPharosUserService(final PharosUserService userService) {
        _userService = userService;
    }

    @Test
    public void testInitialization() {
        final List<User> users = _userService.findAllUsers();
        assertThat(users).isNotNull().isNotEmpty().hasSize(3)
                         .extracting("username").containsExactlyInAnyOrder("admin", "analyst", "reader");

        final List<Group> groups = _userService.findAllGroups();
        assertThat(groups).isNotNull().isNotEmpty().hasSize(3)
                          .extracting("groupName").containsExactlyInAnyOrder("Admins", "Users", "Submitters");

        final List<Submitter> submitters = _userService.findAllSubmitters();
        assertThat(submitters).isNotNull().isEmpty();
    }

    @Test
    public void testUserOperations() {
        final Group admins = _userService.findGroupByName("Admins")
                                         .orElseThrow(() -> new RuntimeException("Couldn't find the Admins group"));

        final List<User> before = _userService.findAllUsers();
        assertThat(before).isNotNull().isNotEmpty().hasSize(3)
                          .extracting("username").containsExactlyInAnyOrder("admin", "analyst", "reader");

        _userService.save(User.builder().username("test1").password("flan").group(admins).build());

        final List<User> after = _userService.findAllUsers();
        assertThat(after).isNotNull().isNotEmpty().hasSize(4)
                         .extracting("username").containsExactlyInAnyOrder("admin", "analyst", "reader", "test1");

        final User test1 = _userService.findUserByName("test1")
                                       .orElseThrow(() -> new RuntimeException("Couldn't find the test1 user I just saved"));
        assertThat(test1).isNotNull()
                         .hasFieldOrPropertyWithValue("username", "test1")
                         .hasFieldOrPropertyWithValue("password", "flan");
        assertThat(test1.getGroups()).hasSize(1).extracting("groupName").containsExactly(admins.getGroupName());
        assertThat(test1.getAuthorities()).hasSize(2).extracting("authority").containsExactlyInAnyOrder("ROLE_ADMIN", "ROLE_USER");
    }

    @Test
    public void testGroupOperations() {
        final List<Group> groups = _userService.findAllGroups();
        assertThat(groups).isNotNull().isNotEmpty().hasSize(3)
                          .extracting("groupName").containsExactlyInAnyOrder("Admins", "Users", "Submitters");
        final Group group = Group.builder().groupName("knaves").authorities(Arrays.asList("ROLE_KNAVE", "ROLE_JACK")).build();
        _userService.save(group);
        final Group knaves = _userService.findGroupByName("knaves").orElseThrow(() -> new RuntimeException("Failed to retrieve group I just created"));
        assertThat(knaves).isNotNull().hasFieldOrPropertyWithValue("groupName", "knaves");
        assertThat(knaves.getAuthorities()).isNotNull().isNotEmpty().hasSize(2).containsExactlyInAnyOrder("ROLE_KNAVE", "ROLE_JACK");
    }
}